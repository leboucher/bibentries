# Computer program/library bibtex entries
It is very hard to find bibtex entries for computer programs, and I think it's
only fair that we be able to cite the developers properly. I am therefore
making a set of `*.bib` files to overcome this problem.

For example, OpenFOAM tells the user to cite either the website or the user.
I took the PDF, got the metadata, and therefore the citation is presented as
Greenshields 2017. Is this what they really want? I think OpenFOAM is bigger
than one person, and probably should be cited as such.

# List of Programs
 - OpenFOAM-5.x (org version)
 - Salome-Meca
